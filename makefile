.PHONY : echo-server echo-client
LDLIBS += -pthread
CC = g++

all: echo-server echo-client

echo-server: echo-server.cpp
	$(CC) -o $@ $< $(LDLIBS) -std=c++17

echo-client: echo-client.cpp
	$(CC) -o $@ $< $(LDLIBS) -std=c++17

clean:
	rm -f echo-client echo-server